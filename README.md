# Nevod Web Site

This repo contains the sources of the official Nevod Web site (https://nevod.io/), which provides playground and
documentation for the open-source [Nevod](https://github.com/nezaboodka/nevod) technology.

The Nevod Web site serves as a demonstration example of the two open-source technologies of the
[Nezaboodka Software](https://nezaboodka.com) company:
- [Nevod](https://github.com/nezaboodka/nevod) - technology for pattern-based text search;
- [Reactronic](https://github.com/nezaboodka/reactronic) - technology for transactionally-reactive programming in TypeScript/JavaScript.

![Nevod Playground](https://raw.githubusercontent.com/nezaboodka/nevod/main/nevod.jpg)

## What is Nevod?

Nevod is a **language** and technology for **pattern-based** text search. It is specially
aimed to rapidly reveal entities and their relationships in texts written in the natural language.

Official project on GitHub: https://github.com/nezaboodka/nevod

Official Website with playground: https://nevod.io/

## What is Reactronic?

Reactronic is a TypeScript/JavaScript library that provides transactionally-reactive state management in a web
application.

Transactional reactivity means that state changes are being made in an isolated data snapshot and then, once atomically
applied, are **consistently propagated** to corresponding visual components for (re)rendering. All that is done
in automatic, seamless, and fine-grained way, because reactronic **takes full care of tracking dependencies**
between visual components (observers) and state (observable objects).

Official project on GitHub: https://github.com/nezaboodka/reactronic

## License

Nevod Web site sources are licensed under [Apache 2.0](LICENSE.txt) license.
