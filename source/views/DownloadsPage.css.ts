//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from './Themes'

export const style = restyler(() => {
  return {
    DownloadGroups: css`
      display: flex;
      flex-direction: row;
      align-items: flex-start;
      justify-content: space-around;
      font-size: 120%;
      padding: 2em;

      @media screen and (max-width: 1100px) {
        flex-direction: column;
        align-items: center;

        .DownloadItem:not(:last-child) {
          margin-bottom: 2ch;
        }
      }
    `,

    DownloadGroup: css`
      padding: 2ch;
      text-align: center;
      min-width: 13ch;
      border: solid 1px ${themes.active.foreground};

      @media screen and (max-width: 1100px) {
        :not(:last-child) {
          margin-bottom: 2ch;
        }
      }
    `,

    DownloadGroupTitle: css`
      margin-bottom: 1.5ch;
    `,

    DownloadLink: css`
      padding: 0.5ch 0;
      display: block;
      background-color: ${themes.active.playgroundFindButtonBackground};
      cursor: pointer;
      border-radius: 0.25em;

      &:not(:last-child) {
        margin-bottom: 1ch;
      }

      &:hover {
        color: ${themes.active.playgroundFindButtonForeground};
        box-shadow: 0 0 0 1px white;
      }
    `,
  }
})
