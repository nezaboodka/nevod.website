//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Customize, Reaction, RxA, RxButton } from 'reactronic-dom'
import { Language, languages } from '../models/Languages'
import { style } from './LanguageButton.css'
import { App } from '../models/App'
import { Transaction } from 'reactronic'

export function LanguageButton(language: Language, app: App, customize?: Customize<HTMLElement>) {
  return (
    RxButton('LanguageButton-' + language.name, null, (e) => {
      e.className = style.class.LanguageButton
      e.innerHTML = language.displayName
      e.onclick = () => Transaction.run(null, () => {
        app.navigation.navigate(app.activePage.getPageLink(language))
      })
      Reaction('ChangeHandler', null, () => {
        e.setAttribute('rx-selected', (language === languages.active) ? 'true' : 'false')
      })
    }).wrapWith(customize)
  )
}
