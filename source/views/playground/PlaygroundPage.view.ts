//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Div, RxDiv, RxImg, RxSpan, Section, Span } from 'reactronic-dom'
import { PlaygroundPage, SearchMonitor } from '../../models/playground/PlaygroundPage'
import { style } from './PlaygroundPage.css'
import { TextLengthField } from './TextLengthField.view'
import { HighlightedTextArea } from './HighlightedTextArea.view'
import { FindButtonView } from './FindButton.view'
import { TagListView } from './TagList.view'
import { cx } from '@emotion/css'
import { ProjectLink } from '../../models/App'
import { languages } from '../../models/Languages'

export function PlaygroundPageView(page: PlaygroundPage) {
  return (
    RxDiv('PlaygroundPageView', null, e => {
      e.className = style.class.PlaygroundPageView
      Div('PatternsTitle', e => {
        e.className = style.class.PatternsTitle
        RxSpan('Span-1', null, () => {
          Span('CustomPatterns', e => {
            e.className = style.class.CustomPatterns
            e.innerHTML = languages.active.playgroundPage_PatternsLabelHtml
          })
        })
        RxSpan('Span-2', null, () => {
          const text = page.searchPatterns
          TextLengthField('PatternsVerificationField', text)
        })
      })
      RxDiv('Patterns', null, e => {
        e.className = style.class.Patterns
        const text = page.searchPatterns
        HighlightedTextArea('Patterns', languages.active.playgroundPage_PatternsAreaPlaceholder, text)
      })
      RxDiv('TextTitle', null, e => {
        e.className = style.class.TextTitle
        Span('Span-1', e => {
          e.textContent = languages.active.playgroundPage_TextToSearchLabel
        })
        const text = page.text
        TextLengthField('TextVerificationField', text)
      })
      RxDiv('Text', null, e => {
        e.className = style.class.Text
        const text = page.text
        HighlightedTextArea('Text', languages.active.playgroundPage_TextAreaPlaceholder, text)
      })
      RxDiv('BasicPatternsDescription', null, e => {
        e.className = cx(style.class.BasicPatternsDescription, style.class.Description)
        e.innerHTML = languages.active.playgroundPage_DescriptionAndExamplesHtml(ProjectLink, page.getPageLink())
      })
      Div('Result', e => {
        e.className = style.class.Result
        Div('ControlsPanel', e => {
          e.className = style.class.ControlsPanel
          Div('FindButtonContainer', e => {
            e.className = style.class.FindButtonContainer
            FindButtonView('FindButton', page)
          })
          RxImg('SearchIndicator', null, e => {
            e.className = style.class.SearchIndicator
            e.setAttribute('rx-active', SearchMonitor.isActive ? 'true' : 'false')
            e.src = './assets/loading.svg'
          })
          Section('Spacer', e => {
            e.style.flexGrow = '1'
          })
          Div('ControlButtonsContainer', e => {
            e.className = style.class.ControlButtonsContainer
            RxSpan('ShowAllButton', null, e => {
              e.className = style.class.ControlButton
              e.textContent = languages.active.playgroundPage_ShowAllButton
              e.onclick = () => { page.showAllTags() }
            })
            RxSpan('HideAllButton', null, e => {
              e.className = style.class.ControlButton
              e.textContent = languages.active.playgroundPage_HideAllButton
              e.onclick = () => { page.hideAllTags() }
            })
          })
        })
        Div('SearchResults', e => {
          e.className = style.class.SearchResults
          RxDiv('SearchResultsScroll', null, e => {
            e.className = style.class.SearchResultsScroll
            const error = page.lastSearchResult.error
            if (error) {
              RxDiv('ErrorMessage', null, e => {
                e.className = style.class.ErrorMessage
                RxDiv('ErrorMessageLabel', null, e => {
                  e.className = style.class.ErrorMessageLabel
                  e.textContent = languages.active.playgroundPage_ErrorLabel
                })
                const error = page.lastSearchResult.error
                if (error) {
                  const textLines = error.message.split('\n')
                  textLines.map((x, i) => {
                    Div(`${i}-${x}`, e => {
                      e.textContent = x
                    })
                  })
                }
              })
            }
            else {
              if (!SearchMonitor.isActive) {
                TagListView('TagList', page)
              }
            }
          })
        })
      })
    })
  )
}
