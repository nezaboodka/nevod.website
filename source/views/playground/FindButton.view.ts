//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Div, RxDiv } from 'reactronic-dom'
import { PlaygroundPage } from '../../models/playground/PlaygroundPage'
import { languages } from '../../models/Languages'
import { style } from './FindButton.css'

export function FindButtonView(id: string, page: PlaygroundPage) {
  return (
    RxDiv(id, null, e => {
      e.className = style.class.FindButton
      e.onclick = async () => {
        await page.search()
      }
      RxDiv('FindLabel', null, e => {
        e.className = style.class.FindLabel
        e.textContent = languages.active.playgroundPage_SearchButton
      })
      // Div('AutoFind', e => {
      //   e.className = style.class.AutoFind
      //   RxDiv('AutoFindLabel', null, e => {
      //     e.className = style.class.AutoFindLabel
      //     e.onclick = (ev) => {
      //       ev.stopPropagation()
      //       page.toggleAutoFindEnabled()
      //     }
      //     RxSpan('CheckMark', null, e => {
      //       e.className = style.class.CheckMark
      //       e.setAttribute('is-enabled', page.isAutoFindEnabled ? 'true' : 'false')
      //     })
      //   })
      // })
    })
  )
}
