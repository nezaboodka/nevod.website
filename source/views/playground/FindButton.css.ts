//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from '../Themes'

export const style = restyler(() => {
  return {
    FindButton: css`
      font-size: 125%;
      display: flex;
      flex-direction: row;
      color: white;
      text-shadow: 0 0 1px gray;
      background-color: ${themes.active.playgroundFindButtonBackground};
      margin-bottom: 1ch;
      margin-right: 0.5ch;
      cursor: pointer;
      box-shadow: 0 0 1px 0 transparent;
      transition: box-shadow 0.3s ease;
      border-radius: 0.25em;
      user-select: none;
      /* .UserSelectNoneMixin(); */

      &:hover {
        box-shadow: 0 0 0 1px white;
      }

      &:focus {
        outline: none;
      }

      &:active {
        background-color: lighten(${themes.active.playgroundFindButtonBackground}, 10%);
      }
    `,

    FindLabel: css`
      flex-grow: 0;
      padding: 0.7ch 1.5ch 0.7ch 1.5ch;
      background-color: transparent;
      display: flex;
      justify-content: center;
      align-items: center;
    `,

    AutoFind: css`
      padding: 0.5ch 1.5ch 0.5ch 0.5ch;
      background-color: transparent;
      display: flex;
      align-items: center;
      /* .UserSelectNoneMixin(); */
    `,

    AutoFindLabel: css`
      display: flex;
      justify-content: center;
      align-items: center;
    `,

    CheckMark: css`
      label: CheckMark;
      height: 1.5ch;
      width: 1.5ch;
      color: transparent;
      background-color: ${themes.active.playgroundFindButtonCheckMarkBackground};
      border: 1px solid grey;
      cursor: pointer;

      ::after {
        content: "";
        display: none;
        border: solid ${themes.active.playgroundFindButtonCheckMarkForeground};
        border-width: 0 0 0.25ch 0.25ch;
        height: 0.45ch;
        width: 1ch;
        margin-top: 0.2ch;
        margin-left: 0.1ch;
        -webkit-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        transform: rotate(-45deg);
      }

      &:hover {
        background-color: ${themes.active.playgroundFindButtonCheckMarkHoverBackground};
      }

      &[is-enabled=true] {
        ::after {
          display: block;
        }
      }
    `,
  }
})
