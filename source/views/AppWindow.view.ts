//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { A, Div, RxA, RxDiv } from 'reactronic-dom'
import { App } from '../models/App'
import { MenuItem } from './MenuItem.view'
import { style } from './AppWindow.css'
import { HomePageView } from './HomePage.view'
import { MarkdownPageView } from './MarkdownPage.view'
import { DownloadPageView } from './DownloadsPage.view'
import { ScrollTopButton } from './ScrollTopButton.view'
import { PlaygroundPageView } from './playground/PlaygroundPage.view'
import { cx } from '@emotion/css'
import { languages } from '../models/Languages'
import { LanguageButton } from './LanguageButton.view'

export function AppWindow(
  app: App) {
  return (
    Div('AppWindow', e => {
      e.className = style.class.AppWindow
      Div('TopLine', e => { e.className = style.class.TopLine })
      Div('Header', e => {
        e.className = style.class.Header
        LogoAndLanguages(app)
        Div('Menu', e => {
          e.className = style.class.Menu
          for (const page of app.pages)
            MenuItem(page)
        })
      })
      RxDiv('Body', null, e => {
        e.className = style.class.Body
        if (app.homePage.isActive)
          HomePageView(app)
        else if (app.tutorialPage.isActive)
          MarkdownPageView(app.tutorialPage)
        else if (app.referencePage.isActive)
          MarkdownPageView(app.referencePage)
        else if (app.downloadsPage.isActive)
          DownloadPageView(app.downloadsPage)
        else if (app.playgroundPage.isActive)
          PlaygroundPageView(app.playgroundPage)
      })
      RxDiv('Footer', null, e => {
        e.className = style.class.Footer
        e.innerHTML = `${languages.active.copyright}<br />
          <a href="https://gitlab.com/nezaboodka/nevod.website" rel=external>https://gitlab.com/nezaboodka/nevod.website</a><br />`
      })
      ScrollTopButton(document.documentElement)
    })
  )
}

function LogoAndLanguages(app: App) {
  return (
    Div('Logo', e => {
      e.className = style.class.Logo
      A('NezaboodkaLogo', e => {
        e.className = style.class.Logo
        e.href = 'https://nezaboodka.com'
        e.innerHTML = '<img src="assets/nezaboodka-logo.svg"/>'
      })
      RxA('NevodLogo', null, e => {
        e.className = cx(style.class.Logo, style.class.NevodLogo)
        e.href = '#/'
        e.innerHTML = languages.active.logoName
      })
      for (const language of languages.all)
        LanguageButton(language, app)
    })
  )
}
