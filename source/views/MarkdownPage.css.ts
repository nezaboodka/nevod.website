//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from './Themes'
import { style as pageStyle } from './Page.css'

export const style = restyler(() => {
  return {
    MarkdownPageView: css`
      font-size: 100%;
      line-height: 1.2;

      h1 {
        font-size: 300%;
        font-weight: bold;
        line-height: 1.2;
        text-align: left;
        text-transform: uppercase;
        padding-left: 1.5ch;
        border-left: 0.12ch solid ${themes.active.menuItemSelectedForeground};
        color: ${themes.active.documentHeading1Foreground};
      }

      h2 {
        font-size: 150%;
        line-height: 1.2;
        margin-top: 1.5em;
        margin-bottom: 0;
        color: ${themes.active.documentHeading2Foreground};
        font-weight: normal;
        text-shadow: 0 0 1px black;
      }

      h2:first-of-type {
        margin-top: 0;
      }

      h3 {
        font-size: 125%;
        line-height: 1.2;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
      }

      code,
      pre {
        font-family: monospace, 'Courier New';
      }

      code {
        font-size: 95%;
        line-height: 1.3;
        color: ${themes.active.documentCodeInlineForeground};
        padding: 0 0.25ch;
        background-color: ${themes.active.documentCodeInlineBackground};
      }

      pre {
        line-height: 1.2;
        overflow: hidden;
        word-wrap: break-word;
        white-space: pre-wrap;
        padding: 0.25em 0.25em;
        background-color: rgba(255, 255, 255, 0.03);
        border: 1px solid rgba(255, 255, 255, 0.07);

        code {
          color: ${themes.active.documentCodeBlockForeground};
          background-color: ${themes.active.documentCodeBlockBackground};
          border: none;
        }
      }

      p {
        font-family: Calibri, Tahoma, Arial, sans-serif;
        margin-top: 0.75em;
        margin-bottom: 0.75em;
      }

      ul {
        list-style-type: disc;
        list-style-position: inside;
        margin-top: 1em;
        margin-bottom: 1em;

        li {
          margin-top: 0.75em;
          margin-bottom: 0.75em;

          p {
            display: inline;
          }
        }
      }
    `,

    TableOfContent: css`
      font-size: 100%;
      overflow: hidden;
      background-color: ${themes.active.documentTOCBackground};
      padding: 1.5em;
      ${pageStyle.class.RightSide};

      h1 {
        font-size: 150%;
        font-weight: normal;
        margin-top: 0.25em;
        margin-bottom: 1em;
        line-height: 1.2;
        text-align: left;
        text-transform: uppercase;
        padding-left: 1.5ch;
        border-left: 0.12ch solid ${themes.active.menuItemSelectedForeground};
        color: ${themes.active.documentHeading1Foreground};
      }

      h2 {
        font-size: 125%;
        line-height: 1.2;
        margin-top: 0.5em;
        margin-bottom: 0;
        font-weight: normal;
        color: ${themes.active.foreground};
      }

      h3 {
        font-size: 100%;
        line-height: 1.2;
        margin-top: 0.5em;
        margin-bottom: 0.5em;
        font-weight: normal;
      }

      a {
        margin-top: 1em;
        margin-bottom: 1em;
      }
    `,
  }
})
