//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { RxDiv } from 'reactronic-dom'
import { MarkdownPage } from '../models/MarkdownPage'
import { style } from './MarkdownPage.css'
import { PageView } from './Page.view'

require('../models/NevodStyle.css')

export function MarkdownPageView(page: MarkdownPage) {
  return (
    PageView(page, (node, e) => {
      node.render()
      RxDiv('MarkdownPageView', null, e => {
        e.className = style.class.MarkdownPageView
        e.innerHTML = page.content
      })
    }, (node, e) => {
      node.render()
      RxDiv('TableOfContent', null, e => {
        e.className = style.class.TableOfContent
        e.innerHTML = page.tocContent
      })
    })
  )
}
