//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Customize, Reaction, RxA } from 'reactronic-dom'
import { Page } from '../models/Page'
import { style } from './MenuItem.css'

export function MenuItem(page: Page, customize?: Customize<HTMLElement>) {
  return (
    RxA('MenuItem-' + page.pagePath, null, e => {
      e.className = style.class.MenuItem
      e.innerHTML = page.menuName
      e.href = page.getPageTopicLink()
      Reaction('ChangeHandler', null, () => {
        e.setAttribute('rx-selected', page.isActive ? 'true' : 'false')
      })
    }).wrapWith(customize)
  )
}
