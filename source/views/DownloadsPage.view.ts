//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Div, A } from 'reactronic-dom'
import { PageView } from './Page.view'
import { style as pageStyle } from './Page.css'
import { style as downloadsStyle } from './DownloadsPage.css'
import { DownloadsPage } from '../models/DownloadsPage'
import { languages } from '../models/Languages'

export function DownloadPageView(page: DownloadsPage) {
  return (
    PageView(page, (node, e) => {
      node.render()
      Div('Description', e => {
        e.className = pageStyle.class.Description
        e.innerHTML = languages.active.downloadsPage_IntroductionHtml
      })
      Div('DownloadGroups', e => {
        e.className = downloadsStyle.class.DownloadGroups
        for (const g of page.groups) {
          Div(`DownloadGroup-${g.title}`, e => {
            e.className = downloadsStyle.class.DownloadGroup
            Div('DownloadGroupTitle', e => {
              e.className = downloadsStyle.class.DownloadGroupTitle
              e.innerText = g.title
            })
            for (const x of g.links) {
              A(x.title, e => {
                e.className = downloadsStyle.class.DownloadLink
                e.innerText = x.title
                e.href = x.link
                e.rel = 'noopener noreferrer'
              })
            }
          })
        }
      })
    }, (node, e) => {
      node.render()
      Div('Picture', e => {
        e.className = pageStyle.class.Picture
        e.innerHTML = '<img src="assets/negrep-3d.png"/>'
      })
    })
  )
}
