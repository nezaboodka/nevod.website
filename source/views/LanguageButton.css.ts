//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { css } from '@emotion/css'
import { restyler } from 'reactronic-dom'
import { themes } from './Themes'

export const style = restyler(() => {
  return {
    LanguageButton: css`
      margin: 0 0.25em;
      border-top: 0.1em solid transparent;
      cursor: pointer;

      &[rx-selected=true] {
        color: ${themes.active.menuItemSelectedForeground};
      }
    `,
  }
})
