//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { nonreactive, reaction } from 'reactronic'
import { Page } from './Page'
import * as MarkdownIt from 'markdown-it'
import * as Token from 'markdown-it/lib/token'
const MarkdownItAnchor = require('markdown-it-anchor').default

import * as prism from 'prismjs'
import 'prismjs/components/prism-nevod'
import { NevodGrammar } from '../models/NevodGrammar'
import { languages } from './Languages'

export class MarkdownPage extends Page {
  contentUrl: string
  tocContent: string

  constructor(pathBase: string) {
    super(pathBase)
    this.contentUrl = ''
    this.tocContent = ''
  }

  @reaction
  async loadContent(): Promise<void> {
    if (this.contentUrl) {
      try {
        const response: Response = await fetch(this.contentUrl, {
          headers: {
            'Content-Type': 'text/markdown; charset=UTF-8',
          },
        })
        const markdownContent = await response.text()
        const pageLink = nonreactive(() => this.getPageLinkWithoutSlash())
        const tocContent: string[] = [languages.active.tableOfContentHtml]
        const refPrefix = pageLink + '/'
        const md = new MarkdownIt({
          highlight: (str: string, lang: string, attrs: string) => {
            const codeHtml = prism.highlight(str, NevodGrammar, 'nevod')
            return `<pre class="dark"><code>${codeHtml}</code></pre>`
          }
        }).use(MarkdownItAnchor, {
          level: [2,3],
          slugify: (s: string) => refPrefix + encodeURIComponent(String(s).trim().toLowerCase().replace(/\s+/g, '-')),
          callback: (token: Token, anchorInfo: { slug: string; title: string }) => {
            tocContent.push(`<${token.tag}><a href="#${anchorInfo.slug}">${anchorInfo.title}</a></${token.tag}>`)
          }
        })
        this.content = md.render(markdownContent)
        this.tocContent = tocContent.join('\n')
      }
      catch (error) {
        console.log(error)
      }
    }
  }
}
