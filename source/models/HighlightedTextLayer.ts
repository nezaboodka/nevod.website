//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { ReactiveObject, reaction, transaction, isnonreactive } from 'reactronic'
import { HslColor } from './NevodServiceApi'
import { Color } from './Color'
import { HighlightedText } from './HighlightedText'
import { themes } from '../views/Themes'

export const DefaultAlpha = 0.35
export const SelectedAlpha = 0.55
export const DimmedAlpha = 0.05
export const NameDimmedAlpha = 0.15
export const BorderSaturation = 100
export const BorderLightness = 30
export const TransparentColor = 'transparent'

export class HighlightedTextLayer extends ReactiveObject {
  owner: HighlightedText
  @isnonreactive readonly name: string
  @isnonreactive readonly count: number
  @isnonreactive readonly html: string
  @isnonreactive readonly baseColor: HslColor

  isSelected: boolean
  isFocused: boolean
  isDimmed: boolean

  constructor(owner: HighlightedText, name: string, count: number, html: string, baseColor: HslColor) {
    super()
    this.owner = owner
    this.name = name
    this.count = count
    this.html = html
    this.baseColor = baseColor
    this.isSelected = true
    this.isFocused = false
    this.isDimmed = false
  }

  @transaction
  toggleSelected(value?: boolean): void {
    if (value === undefined)
      this.isSelected = !this.isSelected
    else
      this.isSelected = value
  }

  @reaction
  protected focusedLayerChanged(): void {
    const focusedLayer = this.owner.focusedTagLayer
    this.isFocused = focusedLayer === this
    this.isDimmed = focusedLayer !== this && focusedLayer !== undefined
  }

  getTextMatchColorString(): string {
    let color: string
    if (this.isSelected) {
      if (this.isFocused)
        color = Color.toHslaString(this.baseColor, SelectedAlpha)
      else {
        if (this.isDimmed)
          color = Color.toHslaString(this.baseColor, DimmedAlpha)
        else
          color = Color.toHslaString(this.baseColor, DefaultAlpha)
      }
    }
    else {
      if (this.isFocused)
        color = Color.toHslaString(this.baseColor, DefaultAlpha)
      else
        color = TransparentColor
    }
    return color
  }

  getTextMatchBorderColorString(): string {
    return Color.toHslaString(this.baseColor, DefaultAlpha, BorderLightness, BorderSaturation)
  }

  getButtonColorString(): string {
    let color: string
    if (this.isSelected) {
      if (this.isFocused)
        color = Color.toHslaString(this.baseColor, SelectedAlpha)
      else {
        if (this.isDimmed)
          color = Color.toHslaString(this.baseColor, NameDimmedAlpha)
        else
          color = Color.toHslaString(this.baseColor, DefaultAlpha)
      }
    } else {
      if (this.isFocused)
        color = Color.toHslaString(this.baseColor, DefaultAlpha)
      else
        color = TransparentColor
    }
    return color
  }

  getButtonBorderColorString(): string {
    let color: string
    if (this.isSelected || this.isFocused)
      color = Color.toHslaString(this.baseColor, 1, BorderLightness, BorderSaturation)
    else
      color = themes.active.playgroundTagButtonInactiveForeground
    return color
  }

  getButtonTextColorString(): string {
    let color: string
    if (this.isSelected || this.isFocused)
      color = themes.active.playgroundTagButtonActiveForeground
    else
      color = themes.active.playgroundTagButtonInactiveForeground
    return color
  }
}
