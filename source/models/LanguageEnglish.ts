//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Language } from './Languages'
import { DefaultPatterns, DefaultText } from './playground/PlaygroundExample-Default'

export class English implements Language {
  readonly name = 'en'

  readonly logoName = 'Nevod'

  readonly displayName = 'English'
  readonly copyright = '© 2017 - 2022 · Nezaboodka Software LLC · Minsk, Belarus'

  readonly homePage_MenuName = 'Home'
  readonly homePage_Title = 'Pattern-Based Text Search'
  homePage_IntroductionHtml(tutorialLink: string, referenceLink: string, playgroundLink: string, downloadsLink: string, projectLink: string): string {
    return `Nevod is a <b>language</b> and an <b>open-source</b> software technology for <b>pattern-based</b> text search. It is specially
    aimed to rapidly reveal entities and their relationships in texts written in the natural language.<br />
    <br />
    Nevod differentiators:<br />
    <br />
    <div><img src="assets/star-plain-blue.svg" />Many times <b>faster</b> than RegExp;</div>
    <div><img src="assets/star-plain-blue.svg" />For <b>ordinary users</b>, not only programmers;</div>
    <div><img src="assets/star-plain-blue.svg" />Scales well on <b>Big Data</b>;</div>
    <div><img src="assets/star-plain-blue.svg" />Operates <b>on words</b>, not characters;</div>
    <div><img src="assets/star-plain-blue.svg" />Matches all patterns in <b>one pass</b>;</div>
    <div><img src="assets/star-plain-blue.svg" />Language <b>neutral</b>;</div>
    <div><img src="assets/star-plain-blue.svg" />Allows <b>patterns based on patterns</b>;</div>
    <div><img src="assets/star-plain-blue.svg" />Supports reusable <b>pattern packages</b>.</div>
    <br />
    Start learning Nevod from reading <a href="${tutorialLink}">tutorial</a> and later refer to the
    language <a href="${referenceLink}">reference</a>.<br />
    <br />
    Try Nevod yourself in the publicly available <a href="${playgroundLink}">playground</a> or
    download <a href="${downloadsLink}">command-line utility</a> for local use.<br />
    <br />
    If you are a software engineer, you may also contribute to the <a href="${projectLink}">open-source Nevod project hosted on GitHub</a>.`
  }

  readonly tableOfContentHtml = '<h1>Content</h1>'

  readonly tutorialPage_MenuName = 'Tutorial'
  readonly tutorialPage_Title = 'Nevod Language Tutorial'

  readonly referencePage_MenuName = 'Reference'
  readonly referencePage_Title = 'Nevod Language Reference'

  readonly downloadsPage_MenuName = 'Downloads'
  readonly downloadsPage_Title = 'Downloads'
  readonly downloadsPage_IntroductionHtml =
    `<p>You can <b>try Nevod yourself</b> by downloading and running the <b>negrep</b> utility.<br/>
    <br/>
    As the name suggests, this utility is equivalent to <b>grep</b> utility, which is a well known command-line
    utility for Linux aimed to search plain-text files for lines that match a regular expression. Negrep serves
    the same purpose, but uses Nevod patterns instead of regular expressions.<br/>
    <br/>
    Negrep is part of the open-source Nevod project hosted on <a href="https://github.com/nezaboodka/nevod">GitHub</a>.
    Below are links for downloading the latest Negrep version for <b>Windows</b>, <b>Linux</b> or <b>macOS</b> as
    self-contained archive that can be unpacked and used immediately without any setup procedure.</p>`

  readonly playgroundPage_MenuName = 'Playground'
  readonly playgroundPage_PatternsLabelHtml = 'Your&nbsp;patterns'
  readonly playgroundPage_TextToSearchLabel = 'Text to search for patterns'
  readonly playgroundPage_TextAreaPlaceholder = 'Enter text'
  readonly playgroundPage_PatternsAreaPlaceholder = 'Enter patterns'

  playgroundPage_DescriptionAndExamplesHtml(projectLink: string, pageLink: string): string {
    return `Learn more about the <b>Basic.np</b> package with basic patterns on
      <a href="${projectLink}">GitHub</a> or try one of the examples:
      <br /><br />
      <div><a href="${pageLink}">Basic</a></div>
      <div><a href="${pageLink}/jobs">Jobs Categorization</a></div>
      <div><a href="${pageLink}/healthcare">Healthcare Info Extraction</a></div>`
  }

  readonly playgroundPage_SearchButton = 'Search'
  readonly playgroundPage_ShowAllButton = 'All'
  readonly playgroundPage_HideAllButton = 'None'
  readonly playgroundPage_ErrorLabel = 'Error:'
  readonly playgroundPage_NoTagsFound = 'No tags found'
  readonly playgroundPage_ReadOnlyLabelHtml = 'read&#8209;only'
  playgroundPage_NumCharactersHtml(num: number): string {
    return `${num}&nbsp;characters`
  }
  playgroundPage_MaxCharactersHtml(num: number, max: number): string {
    return `${num}&nbsp;of&nbsp;${max} characters&nbsp;max`
  }
  readonly playgroundPage_DefaultExample = DefaultPatterns
  readonly playgroundPage_DefaultText = DefaultText
  readonly playgroundPage_PatternDefinitionTooLong = 'Patterns definition is too long'
  readonly playgroundPage_EmptyPatternsDefinition = 'Empty patterns definition'
  readonly playgroundPage_TextToSearchTooLong = 'Text to search for patterns is too long'
  readonly playgroundPage_EmptyTextToSearch = 'Empty text to search for patterns'
}
