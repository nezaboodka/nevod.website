//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

export const DefaultPatterns =
  `@require "Basic.np";
@search Basic.Url;
@search Basic.Date;
@search Basic.PhoneNumber;
@search Basic.Email;

Sentence = Word ... SentenceSeparator;
SentenceSeparator = {End, ".", "!", "?", [2 LineBreak], ~"."+Word};
NevodAndMicrosoft = ("nevod" & "microsoft recognizers") @inside Sentence;
@search NevodAndMicrosoft;
`

export const DefaultText =
  `Nezaboodka Software is a deep tech company established on May 31, 2011
that develops Big Data SaaS products using the proprietary patented technologies,
such as Nevod and NZDB.

Nevod is a language and technology for pattern-based text search. Read more
about comparison of the Nevod vs Microsoft Recognizers in the company blog here:
https://blog.nezaboodka.com/post/2021/599-nevod-vs-microsoft-recognizers-text.

Contact Nezaboodka via email contact@nezaboodka.com or by phone +375(33)333-77-78.
`

export const DefaultPatterns_RU =
  `@требуется "Basic.np";
@искать Basic.Url;
@искать Basic.Date;
@искать Basic.PhoneNumber;
@искать Basic.Email;

Предложение = Слово ... РазделительПредложений;
РазделительПредложений = {Конец, ".", "!", "?", [2 ПереносСтроки], ~"."+Слово};
Невод-MSRecognizers = ("невод" & "microsoft recognizers") @внутри Предложение;
@искать Невод-MSRecognizers;
`

export const DefaultText_RU =
  `ООО «Незабудка Софтвер» - это deeptech-компания (основана 31-05-2011),
разрабатывающая продукты для работы с большими данными на основе собственных
технологий, защищенных патентами. Примерами таких продуктов и технологий являются
технология Невод и СУБД «Незабудка».

«Невод» - это одновременно язык и технология с открытым исходным кодом
для поиска в тексте совпадений с шаблонами. Подробнее о сравнении технологии
Невод и Microsoft Recognizers читайте в блоге компании здесь (на английском):
https://blog.nezaboodka.com/post/2021/599-nevod-vs-microsoft-recognizers-text.

Связаться с Незабудкой можно по почте contact@nezaboodka.com или по тел. +375(33)333-77-78.
`
