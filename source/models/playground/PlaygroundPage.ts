//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { Monitor, ReactiveObject, options, reaction, Reentrance, transaction, isnonreactive } from 'reactronic'
import { SearchRequest, SearchResponse, SearchResult, SearchError, ResultTag, HslColor, ApiUrl } from '../NevodServiceApi'
import { HighlightedText } from '../HighlightedText'
import { Color } from '../Color'
import { Page } from '../Page'
import { languages } from '../Languages'

const MaxPatternsCharCount = 2_000
const MaxTextCharCount = 10_000
const SearchDelayMs = 100
const AutoFindDelayMs = 750
const ErrorColor: HslColor = { h: 0, s: 77, l: 50 }

export const DefaultError: SearchError = {
  message: 'Sorry, something went wrong...'
}

export const NetworkError: SearchError = {
  message: 'Server is too busy or unavailable. Please, try again later.'
}

export const SearchMonitor = Monitor.create('Search Monitor', SearchDelayMs, 0, 1)
export const EditMonitor = Monitor.create('Edit Monitor', 0, AutoFindDelayMs, 1)

export interface PlaygroundData {
  patterns: string
  text: string
}

export interface NevodPackage {
  name: string
  text: string
}

export interface SearchTargets {
  tags: ResultTag[]
  namespaces: ResultTag[]
}

export class ReactiveSearchResult extends ReactiveObject {
  revision: number
  response?: SearchResponse
  error?: SearchError

  constructor() {
    super()
    this.revision = 0
    this.response = undefined
    this.error = undefined
  }
}

export class PlaygroundPage extends Page {
  @isnonreactive readonly customPatterns: HighlightedText
  @isnonreactive readonly text: HighlightedText
  @isnonreactive readonly lastSearchResult: ReactiveSearchResult
  examples: Map<string, PlaygroundData>
  exampleName?: string
  searchPatterns: HighlightedText
  isAutoFindEnabled: boolean
  areCommonPattersIncluded: boolean

  constructor(pathBase: string) {
    super(pathBase)
    this.examples = new Map()
    this.customPatterns = new HighlightedText('', {
      maxLength: MaxPatternsCharCount,
      isNevodPackage: true
    })
    this.text = new HighlightedText('', {
      maxLength: MaxTextCharCount,
      showWhiteSpaces: true
    })
    this.lastSearchResult = new ReactiveSearchResult()
    this.exampleName = undefined
    this.searchPatterns = this.customPatterns
    this.isAutoFindEnabled = false
    this.areCommonPattersIncluded = false
  }

  @transaction
  toggleAutoFindEnabled(value?: boolean): void {
    if (value !== undefined)
      this.isAutoFindEnabled = value
    else
      this.isAutoFindEnabled = !this.isAutoFindEnabled
  }

  @transaction
  toggleCommonPattersIncluded(value?: boolean): void {
    if (value !== undefined)
      this.areCommonPattersIncluded = value
    else
      this.areCommonPattersIncluded = !this.areCommonPattersIncluded
  }

  @transaction
  showAllTags(): void {
    this.text.selectAllLayers()
  }

  @transaction
  hideAllTags(): void {
    this.text.deselectAllLayers()
  }

  canSearch(): boolean {
    return this.text.isValid() && this.searchPatterns.isValid()
  }

  @transaction @options({ monitor: SearchMonitor, reentrance: Reentrance.CancelAndWaitPrevious })
  async search(): Promise<void> {
    this.lastSearchResult.error = undefined
    if (this.canSearch()) {
      try {
        const request: SearchRequest = {
          text: this.text.text
        }
        // if ((this.searchPatterns === this.packagePatterns) && !!this.openedPackage)
        //   request.packageName = this.openedPackage.name
        // else
        request.patterns = this.customPatterns.text
        const requestJson = JSON.stringify(request)
        const httpResponse: Response = await fetch(ApiUrl, {
          headers: {
            'Content-Type': 'application/json',
          },
          body: requestJson,
          method: 'POST',
        })
        const response = await httpResponse.json() as SearchResponse
        this.lastSearchResult.response = response
        if (httpResponse.status === 200 && response.result) {
          this.processSearchResult(response.result)
        }
        else {
          if (response.error)
            this.lastSearchResult.error = response.error
          else
            this.lastSearchResult.error = DefaultError
          this.handleServiceError(this.lastSearchResult.error)
        }
      } catch (error) {
        this.lastSearchResult.error = NetworkError
        console.log(error)
      }
      this.lastSearchResult.revision++
    } else {
      this.handleUserError()
    }
  }

  @reaction
  changeExample(): void {
    if (this.isActive) {
      const subPath = this.topicPath
      this.exampleName = subPath
      const data = this.examples.get(subPath)
      if (data) {
        this.customPatterns.setText(data.patterns)
        this.text.setText(data.text)
        this.lastSearchResult.response = undefined
      }
    }
  }

  @reaction
  protected async autoFind(): Promise<void> {
    if (this.isAutoFindEnabled && !EditMonitor.isActive) {
      this.searchPatterns.text // observe changes
      this.text.text           // observe changes
      await this.search()
    }
  }

  @reaction @options({ monitor: EditMonitor })
  protected textOrPatternsUpdated(): void {
    this.searchPatterns.text // observe changes
    this.text.text           // observe changes
    this.searchPatterns.clearHighlights()
    this.text.clearHighlights()
    this.lastSearchResult.response = undefined
    this.lastSearchResult.error = undefined
  }

  // Internal

  private processSearchResult(searchResult: SearchResult): void {
    const patternHighlights = this.findPositionsOfSearchTargets(this.searchPatterns.text)

    const targetTagCount = patternHighlights.tags.length
    const targetNamespaceCount = patternHighlights.namespaces.length
    const totalTargetCount = targetTagCount + targetNamespaceCount

    const colors: HslColor[] = Color.generate(totalTargetCount)
    const patternColors = colors.slice(targetNamespaceCount)
    const namespaceColors = colors.slice(0, targetNamespaceCount)

    const isPatternFoundPerIndex = new Array<boolean>(targetTagCount)
    const isNamespaceFoundPerIndex = new Array<boolean>(targetNamespaceCount)

    patternHighlights.tags.forEach((pattern, index) => {
      pattern.color = patternColors[index]
      isPatternFoundPerIndex[index] = false
    })
    patternHighlights.namespaces.forEach((ns, index) => {
      ns.color = namespaceColors[index]
      isNamespaceFoundPerIndex[index] = false
    })

    const sortedTags: Array<{ index: number, tag: ResultTag }> = []
    searchResult.tags.forEach(tag => {
      let index
      for (index = 0; index < targetTagCount; index++) {
        const targetTag = patternHighlights.tags[index]
        if (tag.name === targetTag.name || tag.name === targetTag.namespace + '.' + targetTag.name) {
          isPatternFoundPerIndex[index] = true
          tag.color = patternHighlights.tags[index].color
          sortedTags.push({ index, tag })
          break
        }
      }
      if (index === targetTagCount) { // result is from namespace search target
        for (index = 0; index < targetNamespaceCount; index++) {
          const namespace = patternHighlights.namespaces[index]
          if (tag.name.startsWith(namespace.name)) {
            isNamespaceFoundPerIndex[index] = true
            tag.color = namespace.color
            sortedTags.push({ index, tag })
            break
          }
        }
      }
    })
    sortedTags.sort((a, b) => a.index - b.index)
    this.text.updateHighlighting(sortedTags.map(t => t.tag))

    const foundTargetTags = patternHighlights.tags.filter((_, i) => isPatternFoundPerIndex[i])
    const foundTargetNamespaces = patternHighlights.namespaces.filter((_, i) => isNamespaceFoundPerIndex[i])
    const tags: ResultTag[] = foundTargetTags.concat(foundTargetNamespaces)
    this.searchPatterns.updateHighlighting(tags)
  }

  private findPositionsOfSearchTargets(patternsText: string): SearchTargets {
    const namespaceKeyword_EN = 'namespace'
    const namespaceKeyword_RU = 'пространство'
    const searchKeyword_EN = 'search'
    const searchKeyword_RU = 'искать'
    const patternKeyword_EN = 'pattern'
    const patternKeyword_RU = 'шаблон'
    const result: SearchTargets = {
      tags: [],
      namespaces: [],
    }
    let currentNamespace: string = ''
    let isNamespaceTarget: boolean = false
    let prev: string
    let curr: string = '\0'
    if (patternsText.length > 0) {
      let i = 0
      while (i < patternsText.length) {
        prev = curr
        curr = patternsText[i]
        i++
        switch (curr) {
          case '\"': { // skip "string" literal
            while (i < patternsText.length && patternsText[i] !== '\"')
              i++
            curr = patternsText[i]
            i++
            break
          }
          case '\'': { // skip 'string' literal
            while (i < patternsText.length && patternsText[i] !== '\'')
              i++
            curr = patternsText[i]
            i++
            break
          }
          case '/': { // skip single line comment
            if (prev === '/') {
              while (i < patternsText.length && patternsText[i] !== '\n')
                i++
              curr = patternsText[i]
              i++
            }
            break
          }
          case '*': { // skip multiline comment
            if (prev === '/') {
              while (i < patternsText.length && patternsText[i] !== '/' && patternsText[i - 1] !== '*')
                i++
              curr = patternsText[i]
              i++
            }
            break
          }
          case '@': { // try to get current namespace
            let rest: string = patternsText.substring(i)
            if (rest.startsWith(namespaceKeyword_EN) || rest.startsWith(namespaceKeyword_RU)) {
              if (rest.startsWith(namespaceKeyword_EN))
                i += namespaceKeyword_EN.length
              else
                i += namespaceKeyword_RU.length
              while (i < patternsText.length && patternsText[i].match(/\s/)) {
                i++
              }
              const nStart = i
              while (i < patternsText.length && !patternsText[i].match(/[\s{]/)) {
                i++
              }
              currentNamespace = patternsText.substring(nStart, i)
            } else if (rest.startsWith(searchKeyword_EN) || rest.startsWith(searchKeyword_RU)) {
              if (rest.startsWith(searchKeyword_EN))
                i += searchKeyword_EN.length
              else
                i += searchKeyword_RU.length
              while (i < patternsText.length && patternsText[i].match(/\s/)) {
                i++
              }
              if (patternsText[i] == '@') { // skip optional @pattern
                i++
                rest = patternsText.substring(i)
                if (rest.startsWith(patternKeyword_EN)) {
                  i += patternKeyword_EN.length
                } else if (rest.startsWith(patternKeyword_RU)) {
                  i += patternKeyword_RU.length
                }
              }
              ({ i, isNamespaceTarget } = this.saveSearchTarget(i, patternsText, isNamespaceTarget, result,
                currentNamespace))
              curr = patternsText[i]
              i++
            }
            break
          }
          case '#': { // search target found
            ({ i, isNamespaceTarget } = this.saveSearchTarget(i, patternsText, isNamespaceTarget, result,
              currentNamespace))
            curr = patternsText[i]
            i++
            break
          }
          default: {
            // do nothing
            break
          }
        }
      }
    }
    result.namespaces.sort((a, b) => b.name.length - a.name.length) // sort from longest to shortest
    return result
  }

  private saveSearchTarget(i: number, patternsText: string, isNamespaceTarget: boolean, result: SearchTargets,
    currentNamespace: string): { i: number, isNamespaceTarget: boolean } {

    while (i < patternsText.length && patternsText[i].match(/\s/)) {
      i++
    }
    const start = i
    while (i < patternsText.length && !patternsText[i].match(/[\s=(;*]/)) {
      i++
    }
    const targetName: string = patternsText.substring(start, i)
    let end = i
    if (patternsText[i] === '*') {
      isNamespaceTarget = true
      end++
      i++
    }
    while (i < patternsText.length && patternsText[i].match(/\s/)) {
      i++
    }
    if (i < patternsText.length && patternsText[i].match(/[=(;*]/)) {
      const target: ResultTag = {
        name: targetName,
        positions: [{ start, length: end - start }],
      }
      if (isNamespaceTarget) {
        result.namespaces.push(target)
        isNamespaceTarget = false
      }
      else { // search target is tag
        if (currentNamespace)
          target.namespace = currentNamespace
        result.tags.push(target)
      }
    }
    return { i, isNamespaceTarget }
  }

  private handleServiceError(error: SearchError): void {
    if (error.position !== undefined && error.position !== null) {
      const errorTag: ResultTag = {
        name: 'Error',
        positions: [
          {
            start: error.position,
            length: 1,
          },
        ],
        color: ErrorColor,
      }
      this.searchPatterns.updateHighlighting([errorTag])
    }
  }

  private handleUserError(): void {
    let errorMessage: string = ''
    const language = languages.active
    if (!this.searchPatterns.isValid()) {
      if (this.searchPatterns.isOverflown())
        errorMessage = language.playgroundPage_PatternDefinitionTooLong
      else
        errorMessage = language.playgroundPage_EmptyPatternsDefinition
    }
    if (!this.text.isValid()) {
      if (errorMessage.length > 0)
        errorMessage += '\n\n'
      if (this.text.isOverflown())
        errorMessage += language.playgroundPage_TextToSearchTooLong
      else
        errorMessage += language.playgroundPage_EmptyTextToSearch
    }
    this.lastSearchResult.error = {
      message: errorMessage,
    }
  }
}
