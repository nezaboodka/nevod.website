//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

export const JobsPatterns =
`#Target = (Line @having Nursing) @outside (Line @having Management);
Line = Word ... {End, LineBreak};
Nursing = {"nurse", "nursing"};
Management = {"director", "recruiter", CXO};
CXO = "chief" .. [0-3] .. "officer";`

export const JobsText =
`Private duty nurse
Executive Nurse Recruiter
Director of Nursing Services
Licensed practical nurse
Chief Nursing Officer
Pediatric night nurse`
