//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

export const HealthcarePatterns =
`#EjectionFraction = Name _ ?"by visual inspection" _ Qualifier _ Value;
#Name = {"ejection fraction", "LVEF"};
#Qualifier = {"is", "of"} + ?(Space + {"at least", "about", "greater than", "less than", "equal to"});
#Value = {LinguisticValue, Num + ?("-" + Num)};
#LinguisticValue = {"normal", "moderate", "severe"};`

export const HealthcareText =
`ejection fraction is at least 70-75
ejection fraction of about 20
ejection fraction of 60
ejection fraction of greater than 65
ejection fraction of 55
ejection fraction by visual inspection is 65
LVEF is normal`
