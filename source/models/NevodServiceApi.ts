//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

export const ApiUrl = 'https://nevod.nezaboodka.com/api/v2/search'

export interface HslColor {
  h: number
  s: number
  l: number
}

export interface SearchRequest {
  patterns?: string
  packageName?: string
  text: string
}

export interface SearchResponse {
  result?: SearchResult
  error?: SearchError
}

export interface SearchResult {
  tags: ResultTag[]
}

export interface ResultTag {
  name: string
  positions: TextPosition[]
  namespace?: string
  color?: HslColor
}

export interface TextPosition {
  start: number
  length: number
}

export interface SearchError {
  message: string
  position?: number  // syntax error
}
