//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { ReactiveObject, reaction, transaction } from 'reactronic'
import { Color } from './Color'
import { HighlightedTextLayer } from './HighlightedTextLayer'
import * as prism from 'prismjs'
import { TextPosition, ResultTag } from './NevodServiceApi'
import { NevodGrammar } from './NevodGrammar'

// const NewLineSymbol = '↵';
const NewLineSymbol = '⤶'
const EndOfTextSymbol = '⇤'
const NonWhiteSpacesRegExp = RegExp(`[^\\s${NewLineSymbol}${EndOfTextSymbol}]`, 'g')

export interface HighlightedTextOptions {
  isReadOnly?: boolean
  maxLength?: number
  isNevodPackage?: boolean
  showWhiteSpaces?: boolean
}

export interface TextSize {
  width: number
  height: number
}

export class HighlightedText extends ReactiveObject {
  syntaxLayerHtml: string
  whiteSpacesLayerHtml: string
  text: string
  textSize: TextSize
  isReadOnly: boolean
  maxLength: number
  isNevodPackage: boolean
  showWhiteSpaces: boolean
  tagLayers: HighlightedTextLayer[]
  focusedTagLayer?: HighlightedTextLayer

  constructor(text: string, options: HighlightedTextOptions = {}) {
    super()
    this.text = text
    this.textSize = {
      width: 0,
      height: 0
    }
    this.isReadOnly = options.isReadOnly !== undefined ? options.isReadOnly : false
    this.maxLength = options.maxLength !== undefined ? options.maxLength : 0
    this.isNevodPackage = options.isNevodPackage ?? false
    this.showWhiteSpaces = options.showWhiteSpaces !== undefined ? options.showWhiteSpaces : false
    this.syntaxLayerHtml = ''
    this.whiteSpacesLayerHtml = ''
    this.tagLayers = []
    this.focusedTagLayer = undefined
  }

  isValid(): boolean {
    return (this.text.length > 0) && !this.isOverflown()
  }

  isOverflown(): boolean {
    return this.maxLength > 0 && (this.text.length > this.maxLength)
  }

  hasTagHighlighting(): boolean {
    return this.tagLayers.length > 0
  }

  @transaction
  setText(text: string): void {
    this.text = text
  }

  @transaction
  updateHighlighting(tags: ResultTag[]): void {
    this.tagLayers = tags.map(tag =>
      new HighlightedTextLayer(this, tag.name, tag.positions.length,
        HighlightedText.getHighlightingHtml(this.text, tag.positions), tag.color || Color.createNew(Color.default)))
  }

  @transaction
  selectAllLayers(): void {
    this.tagLayers.forEach(x => x.toggleSelected(true))
  }

  @transaction
  deselectAllLayers(): void {
    this.tagLayers.forEach(x => x.toggleSelected(false))
  }

  @transaction
  clearHighlights(): void {
    this.tagLayers = []
  }

  @transaction
  focusLayer(layer: HighlightedTextLayer): void {
    this.focusedTagLayer = layer
  }

  @transaction
  unfocusLayer(): void {
    this.focusedTagLayer = undefined
    // this.focusedTagLayer = this.tagLayers[0]
  }

  @reaction
  protected textUpdated(): void {
    const text  = this.text
    const lines = this.text.split('\n')
    const height = lines.length
    const width = lines.reduce(((max, line) => line.length > max.length ? line : max), '').length
    this.textSize = {
      width,
      height
    }
    if (this.isNevodPackage) {
      const html = prism.highlight(text, NevodGrammar, 'nevod')
      this.syntaxLayerHtml = html
    }
    if (this.showWhiteSpaces) {
      let html = text.replace(/([ \t])(?=\n)/gm, '$1' + NewLineSymbol)
      html = html.replace(/([ \t\n])$/, '$1' + EndOfTextSymbol)
      if (html.endsWith('\n')) {
        html += '\n'
      }
      html = html.replace(NonWhiteSpacesRegExp, ' ')
      this.whiteSpacesLayerHtml = html
    }
  }

  private static getHighlightingHtml(text: string, positions: TextPosition[]): string {
    const html: string[] = []
    let previousEnd = 0
    let currentPart: string
    text = text.replace(NonWhiteSpacesRegExp, ' ')
    for (const p of positions) {
      currentPart = text.slice(previousEnd, p.start)
      html.push(currentPart)
      html.push('<mark>')
      const end = p.start + p.length
      currentPart = text.slice(p.start, end)
      currentPart = currentPart.replace(/\n/g, NewLineSymbol + '\n')
      html.push(currentPart)
      if (end > text.length) {
        html.push(EndOfTextSymbol)
      }
      html.push('</mark>')
      previousEnd = end
    }
    currentPart = text.slice(previousEnd)
    html.push(currentPart)
    let result = html.join('')
    if (result.endsWith('\n')) {
      result += '\n'
    }
    return result
  }
}
