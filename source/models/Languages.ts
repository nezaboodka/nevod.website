//--------------------------------------------------------------------------------------------------
// Copyright © Nezaboodka™ Software LLC. All rights reserved.
// Licensed under the Apache License, Version 2.0.
//--------------------------------------------------------------------------------------------------

import { ReactiveObject, Transaction, transaction, isnonreactive } from 'reactronic'
import { Russian } from './LanguageRussian'
import { English } from './LanguageEnglish'

export interface Language {
  readonly name: string

  readonly logoName: string

  readonly displayName: string
  readonly copyright: string

  readonly homePage_MenuName: string
  readonly homePage_Title: string
  homePage_IntroductionHtml(tutorialLink: string, referenceLink: string, playgroundLink: string, downloadsLink: string, projectLink: string): string

  readonly tableOfContentHtml: string

  readonly tutorialPage_MenuName: string
  readonly tutorialPage_Title: string

  readonly referencePage_MenuName: string
  readonly referencePage_Title: string

  readonly downloadsPage_MenuName: string
  readonly downloadsPage_Title: string
  readonly downloadsPage_IntroductionHtml: string

  readonly playgroundPage_MenuName: string
  readonly playgroundPage_PatternsLabelHtml: string
  readonly playgroundPage_TextToSearchLabel: string
  readonly playgroundPage_TextAreaPlaceholder: string
  readonly playgroundPage_PatternsAreaPlaceholder: string
  playgroundPage_DescriptionAndExamplesHtml(projectLink: string, pageLink: string): string
  readonly playgroundPage_SearchButton: string
  readonly playgroundPage_ShowAllButton: string
  readonly playgroundPage_HideAllButton: string
  readonly playgroundPage_ErrorLabel: string
  readonly playgroundPage_NoTagsFound: string
  readonly playgroundPage_ReadOnlyLabelHtml: string
  playgroundPage_NumCharactersHtml(current: number): string
  playgroundPage_MaxCharactersHtml(current: number, max: number): string
  readonly playgroundPage_DefaultExample: string
  readonly playgroundPage_DefaultText: string
  readonly playgroundPage_PatternDefinitionTooLong: string
  readonly playgroundPage_EmptyPatternsDefinition: string
  readonly playgroundPage_TextToSearchTooLong: string
  readonly playgroundPage_EmptyTextToSearch: string
}

export class Languages extends ReactiveObject {
  @isnonreactive readonly all: Language[]
  active: Language

  constructor (...languages: Language[]) {
    super()
    this.all = []
    languages.forEach(x => this.all.push(x))
    this.active = languages[0]
  }

  @transaction
  setActive(value: Language): void {
    this.active = value
  }

  @transaction
  setActiveByName(name: string): void {
    const i = this.all.findIndex(t => t.name === name)
    if (i >= 0)
      this.active = this.all[i]
  }

  @transaction
  setNextActive(): void {
    const i = (this.all.indexOf(this.active) + 1) % this.all.length
    const language = this.all[i]
    this.active = language
  }
}

export const languages = Transaction.run(null, () => new Languages(new English(), new Russian()))
